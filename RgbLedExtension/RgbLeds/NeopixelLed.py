import board
import neopixel


from .RgbLed import RgbLed

class NeopixelLed(RgbLed):
    def __init__(self):

        self.neopixel_led = neopixel.NeoPixel(board.D18,1, pixel_order = neopixel.RGB)
        super().__init__()

    def write_to_led(self):
        self.neopixel_led[0] = self.state_rgb
        self.neopixel_led.show()
