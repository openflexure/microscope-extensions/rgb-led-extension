import numpy as np
from labthings import fields, find_component, find_extension
from labthings.schema import Schema
from labthings.views import ActionView, PropertyView, View

class RgbLedView(ActionView):
    args = {
        "red": fields.Integer(missing=0,allow_none=True),
        "green": fields.Integer(missing=0,allow_none=True),
        "blue": fields.Integer(missing=0,allow_none=True),
    }

    def post(self,args):
        color = np.array(
            [args.get("red") or 0, args.get("green") or 0, args.get("blue") or 0]
        )

        rgb_led_extension = find_extension(
            "org.openflexure.rgb-led"
        )

        rgb_led_extension.set_rgb_led(color[0], color[1],color[2])

