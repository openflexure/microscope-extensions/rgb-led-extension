from labthings.extensions import BaseExtension
from openflexure_microscope.api.utilities.gui import build_gui

from .RgbLedView import *
from .RgbLeds.NeopixelLed import NeopixelLed
from .RgbLeds.RgbLed import RgbLed

class RgbLedExtension(BaseExtension):
    def __init__(self):
        super().__init__(
            "org.openflexure.rgb-led",
            version = "0.0.1",
        )

        self.rgb_led = NeopixelLed()

        self.color_picker = dict(red =0, blue=0, green=0)

        self.script_commands = {
            "rgb_led": {
                "function": RgbLedExtension.set_rgb_led,
                "description": "Set the RGB LED illumination",
                "args": [
                    "int:Red (0-255)",
                    "int:Green (0-255)",
                    "int:Blue (0-255)",
                ],
                "kwargs": {},
                "locks": [],
            }
        }


        def gui_func(self):
            return {
                "icon": "traffic",
                "forms": [
                    {
                        "name": "Control an RGB Neopixel LED",
                        "route": "/set-rgb-led",
                        "isTask": True,
                        "isCollapsible": True,
                        "submitLabel": "Set colour",
                        "schema": [
                            {
                                "fieldType": "numberInput",
                                "name": "red",
                                "label": "Red",
                                "min": 0,
                                "default": self.color_picker["red"],
                                "max": 255,
                            },
                            {
                                "fieldType": "numberInput",
                                "name": "green",
                                "label": "Green",
                                "min": 0,
                                "default": self.color_picker["green"],
                                "max": 255,
                            },
                            {
                                "fieldType": "numberInput",
                                "name": "blue",
                                "label": "Blue",
                                "min": 0,
                                "default": self.color_picker["blue"],
                                "max": 255,
                            },
                        ]
                    }
                ],
            }

        self.add_view(RgbLedView, "/set-rgb-led")
        self.add_meta("gui", build_gui(gui_func(self), self))
        
    def save_color_picker(self, red, green, blue):
        self.color_picker["red"] = red
        self.color_picker["green"] = green
        self.color_picker["blue"] = blue

 
    def set_rgb_led(self, red, green, blue):
        self.save_color_picker(red,green,blue)
        self.rgb_led.set_state_rgb([red,green,blue])
        self.rgb_led.write_to_led()
        return



